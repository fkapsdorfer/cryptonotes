/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file This file loads and exports the database.
 */

'use strict'

var Nedb    = require('nedb');
var CONFIG  = require('../../libs/config');

module.exports = new Nedb({ inMemoryOnly: true, autoload: true });
