/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file This is a unit test for SecureRecord model.
 */

'use strict';

var assert          = require('assert');
var rewire          = require('rewire');
var async           = require('async');
var SecureRecord    = rewire('../models/SecureRecord');
var db              = require('./mocks/db');
var Errors          = require('../libs/Errors').SecureRecord;

SecureRecord.__set__('db', db);


describe('SecureRecord.setIdStr()', () => {
    var sr = new SecureRecord();

    it('Should throw Errors.SecureRecord.InvalidIdStr if empty string given', () => {
        assert.throws(sr.setIdStr.bind(sr, ''), Errors.InvalidIdStr);
    });

    it('Should throw Errors.SecureRecord.InvalidIdStr if null given', () => {
        assert.throws(sr.setIdStr.bind(sr, null), Errors.InvalidIdStr);
    });

    it('Should throw Errors.SecureRecord.InvalidIdStr if undefined given', () => {
        assert.throws(sr.setIdStr.bind(sr, undefined), Errors.InvalidIdStr);
    });

    it('Should be ok if string given', () => {
        assert.doesNotThrow(sr.setIdStr.bind(sr, '1'));
    });
    // Other types then string will result in undefined behaviour
});


describe('SecureRecord.setKeyHash()', () => {
    var sr = new SecureRecord();

    it('Should pass Errors.SecureRecord.InvalidKey if empty string given', (done) => {
        sr.setKeyHash('', (err) => {
            assert.ok(err instanceof Errors.InvalidKey);
            done();
        });
    });

    it('Should pass Errors.SecureRecord.InvalidKey if string length < 8', (done) => {
        sr.setKeyHash('1234567', (err) => {
            assert.ok(err instanceof Errors.InvalidKey);
            done();
        });
    });

    it('Should pass Errors.SecureRecord.InvalidKey if null given', (done) => {
        sr.setKeyHash(null, (err) => {
            assert.ok(err instanceof Errors.InvalidKey);
            done();
        });
    });

    it('Should pass Errors.SecureRecord.InvalidKey if undefined given', (done) => {
        sr.setKeyHash(undefined, (err) => {
            assert.ok(err instanceof Errors.InvalidKey);
            done();
        });
    });

    it('Should be ok if string longer or equal than 8 characters given', (done) => {
        sr.setKeyHash('12345678', (err) => {
            assert.equal(err, undefined);
            done();
        });
    });
});


describe('SecureRecord.setKeyHash() & SecureRecord.getKeyHash()', (done) => {
    var sr = new SecureRecord();

    it('Getter should return the same value as is stored', (done) => {
        sr.setKeyHash('12345678', (err) => {
            assert.equal(sr.getKeyHash(), sr._keyHash);
            done();
        });
    });

    it('Getter should not return the same value that was given to the setter (hashing)', (done) => {
        sr.setKeyHash('12345678', (err) => {
            assert.notEqual(sr.getKeyHash(), '12345678');
            done();
        });
    });
});


describe('SecureRecord.isKeyMatching()', (done) => {
    var sr = new SecureRecord();

    it('Should result in identical hashes', (done) => {
        sr.setKeyHash('12345678', (err) => {
            assert.equal(err, undefined);
            sr.isKeyMatching('12345678', (err, result) => {
                assert.equal(err, undefined);
                assert.ok(result);
                done();
            });
        });
    });

    it('Should result in non identical hashes', (done) => {
        sr.setKeyHash('12345678', (err) => {
            assert.equal(err, undefined);
            sr.isKeyMatching('87654321', (err, result) => {
                assert.equal(err, undefined);
                assert.ok(!result);
                done();
            });
        });
    });
});


describe('SecureRecord.setData()', (done) => {
    var sr = new SecureRecord();

    it('Setting data without valid key should pass Errors.SecureRecord.InvalidKey', (done) => {
        sr.setData('Some data to be encrypted', null, (err) => {
            assert.ok(err instanceof Errors.InvalidKey);
            done();
        });
    });

    it('Setting data without valid key should pass Errors.SecureRecord.InvalidKey', (done) => {
        sr.setData('Some data to be encrypted', '1234567', (err) => {
            assert.ok(err instanceof Errors.InvalidKey);
            done();
        });
    });

    it('Should be ok', (done) => {
        sr.setData('Some data to be encrypted', '12345678', (err) => {
            assert.equal(err, undefined);
            done();
        });
    });
});


describe('SecureRecord.getData()', (done) => {
    var sr = new SecureRecord();
    var key = '12345678';
    var data = 'Some data to be encrypted';

    before((done) => {
        async.waterfall([
            sr.setKeyHash.bind(sr, key),
            sr.setData.bind(sr, data, key)
        ], (err) => {
            if (err) {
                throw err;
            }
            done();
        });
    });

    it('Should pass Errors.SecureRecord.InvalidKey if null key given', (done) => {
        sr.getData(null, (err) => {
            assert.ok(err instanceof Errors.InvalidKey);
            done();
        });
    });

    it('Should pass Errors.SecureRecord.InvalidKey if key length < 8', (done) => {
        sr.getData(null, (err) => {
            assert.ok(err instanceof Errors.InvalidKey);
            done();
        });
    });

    it('Getting data without matching key should pass Errors.SecureRecord.KeysNotMatching', (done) => {
        sr.getData('87654321', (err) => {
            assert.ok(err instanceof Errors.KeysNotMatching);
            done();
        });
    });

    it('Should be ok', (done) => {
        sr.getData(key, (err, result) => {
            assert.equal(result, data);
            done();
        });
    });
});


describe('SecureRecord.persist()', (done) => {
    var sr;
    var idStr = 'AnIdentifier';
    var key = '12345678';
    var data = 'Some data to be encrypted';

    before((done) => {
        sr = new SecureRecord(idStr, key, data, (err) => {
            if (err) {
                throw err;
            }
            done();
        });
    });

    afterEach((done) => {
        db.remove({}, { multi: true }, (err) => {
            if (err) {
                throw err;
            }
            done();
        });
    });


    it('Should persist successfully', (done) => {
        sr.persist((err) => {
            assert.equal(err, undefined);
            done();
        });
    });

    it('Should actually persist according to specified model format', (done) => {
        async.waterfall([
            sr.persist.bind(sr),
            db.findOne.bind(db, { idStr: idStr })
        ], (err, doc) => {
            assert.equal(err, undefined);
            assert.ok(doc.idStr);
            assert.ok(doc.keyHash);
            assert.ok(doc.data);
            done();
        });
    });

    it('Existing record (by idStr) should be updated', (done) => {
        async.waterfall([
            sr.persist.bind(sr),
            sr.persist.bind(sr),
            db.count.bind(db, {})
        ], (err, count) => {
            assert.equal(err, undefined);
            assert.strictEqual(count, 1);
            done();
        });
    });
});


describe('SecureRecord.fetchByIdStr()', (done) => {
    var sr = new SecureRecord();
    var idStr = 'AnIdentifier';
    var key = '12345678';
    var keyHash;
    var data = 'Some data to be encrypted';

    before((done) => {
        let sr;

        sr = new SecureRecord(idStr, key, data, (err) => {
            async.waterfall([
                sr.persist.bind(sr)
            ], (err) => {
                if (err) {
                    throw err;
                }
                keyHash = sr.getKeyHash();
                done();
            });
        });
    });


    it('Should fetch whithout any error', (done) => {
        sr.fetchByIdStr(idStr, (err) => {
            assert.equal(err, undefined);
            done();
        });
    });

    it('The fetched values should be correct', (done) => {
        async.waterfall([
            sr.fetchByIdStr.bind(sr, idStr),
            (cb) => {
                assert.equal(sr.getIdStr(), idStr);
                assert.equal(sr.getKeyHash(), keyHash);
                sr.getData(key, cb);
            }
        ], (err, res) => {
            assert.equal(err, undefined);
            assert.equal(res, data);
            done();
        });
    });
});
