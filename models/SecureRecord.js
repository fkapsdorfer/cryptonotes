/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file
 * This module provides class SecureRecord which is an interface to persistence.
 * It contains only encrypted SecureRecord-_data, hashed SecureRecord-_keyHash.
 * The setters accepts unencrypted/unhashed data. The setters do their validation.
 * The getters returns only unencrypted data.
 */

'use strict'

var ALGORITHM = 'aes256';

var async   = require('async');
var bcrypt  = require('bcrypt-nodejs');
var crypto  = require('crypto');
var Errors  = require('../libs/Errors').SecureRecord;
var db      = require('../libs/db');


/**
 * Set none or all. For further format description see the coresponding setters.
 *
 * @param {string} [idStr]
 * @param {string} [key]
 * @param {string} [data]
 * @param {SecoureRecord.constructCb} [callback]
 */
function SecureRecord (idStr, key, data, callback) {

    /**
     * An identifeier used to find records
     *
     * @var {string}
     * @private
     */
    this._idStr     = null;

    /**
     * The hashed key used to encrypt the data
     *
     * @var {string}
     * @private
     */
    this._keyHash   = null;

    /**
     * Encrypted data
     *
     * @var {string}
     * @private
     */
    this._data      = null;


    // construct
    if (data != null) {
        try {
            this.setIdStr(idStr);
        } catch (err) {
            return callback(err);
        }
        async.waterfall([
            this.setKeyHash.bind(this, key),
            this.setData.bind(this, data, key)
        ], callback);
    }
}
/**
 * @callback SecureRecord.constructCb
 * @param {Errors} [err]  See coresponding setters for different errors.
 */





/**
 * @param {string} idStr
 * @throws {Errors.SecureRecord.InvalidIdStr}
 */
SecureRecord.prototype.setIdStr = function (idStr)
{
    if (!idStr) {
        throw new Errors.InvalidIdStr();
    }
    this._idStr = idStr;
}


/**
 * @param {string}                      key         Unhashed key
 * @param {SecureRecord.setKeyHashCb}   callback
 */
SecureRecord.prototype.setKeyHash = function (key, callback)
{
     if (!SecureRecord._isKeyValid(key)) {
        return callback(new Errors.InvalidKey())
    }
     SecureRecord._createHash(key, (err, hash) => {
        if (err) {
            return callback(new Errors.CannotCreateHash(err));
        }

        this._keyHash = hash;
        return callback();
    });
}
/**
 * @callback SecureRecord.setKeyHashCb
 * @param {Errors.SecureRecord.InvalidKey|Errors.SecureRecord.CannotCreateHash} [err]
 */


/**
 * @param {string}                     data        Unencrypted data to store
 * @param {string}                     key         Unhashed key
 * @param {SecureRecord.setDataCb}     callback
 */
SecureRecord.prototype.setData = function (data, key, callback)
{
    if (!SecureRecord._isKeyValid(key)) {
        return callback(new Errors.InvalidKey());
    }
    this._data  = SecureRecord._encrypt(data, key);
    return callback();  // It may be intuitive to be asynchronous
}
/**
 * @callback SecureRecord.setDataCb
 * @param {Errors.SecureRecord.InvalidKey} [err]
 */





/**
 * @returns {string}
 */
SecureRecord.prototype.getIdStr = function ()
{
    return this._idStr;
}


/**
 * @returns {string}    Hashed key
 */
SecureRecord.prototype.getKeyHash = function ()
{
    return this._keyHash;
}


/**
 * @param {string}                      key          Unhashed key
 * @param {SecoureRecord.getDataCb}     callback
 */
SecureRecord.prototype.getData = function (key, callback)
{
    var self = this;

    if (!SecureRecord._isKeyValid(key)) {
        return callback(new Errors.InvalidKey());
    }

    this.isKeyMatching(key, (err, result) => {
        if (err)        return callback(err); // err is of Errors.SecureRecord.CannotCompareHashes
        if (!result)    return callback(new Errors.KeysNotMatching(err));

        return callback(null, SecureRecord._dencrypt(self._data, key));
    });
}
/**
 * @callback SecureRecord.getDataCb
 * @param {Errors.SecureRecord.Invalid|Errors.SecureRecord.CannotCompareHashes|Errors.SecureRecord.KeysNotMatching} [err]
 * @param {string} data     Unencrypted data
 */





/**
 * @param {string}                              key          Unhashed key
 * @param {SecoureRecord.isKeyMatchingCb}       callback
 */
SecureRecord.prototype.isKeyMatching = function (key, callback)
{
    return SecureRecord._compareHashes(key, this._keyHash, (err, result) => {
        if (err) {
            return callback(new Errors.CannotCompareHashes);
        }
        return callback(null, result);
    });
}
/**
 * @callback SecureRecord.isKeyMatchingCb
 * @param {Errors.SecureRecord.CannotCompareHashes} [err]
 * @param {boolean} result
 */





/**
 * @param {SecoureRecord.persistCb} callback
 */
SecureRecord.prototype.persist = function (callback)
{
    async.waterfall([
        db.findOne.bind(db, { idStr: this._idStr }),
        (doc, cb) => {
            // If record already exists - update
            if (doc) {
                return db.update(
                    { idStr: this._idStr },
                    { $set: { data: this._data, keyHash: this._keyHash } },
                    { multi: false },
                    cb
                );
            }
            // If this is a new record
            return db.insert({ idStr: this._idStr, keyHash: this._keyHash, data: this._data }, cb);
        }
    ], (err) => {
        if (err) {
            err = new Errors.DbError(err);
        }
        return callback(err);
    });
}
/**
 * @callback SecureRecord.persistCb
 * @param {Errors.SecureRecord.DbError} [err]
 */


/**
 * @param {string} idStr
 * @param {SecoureRecord.fetchByIdStrCb} callback
 */
SecureRecord.prototype.fetchByIdStr = function (idStr, callback) {
    var self = this;

    try {
        this.setIdStr(idStr);
    } catch (err) {
        return callback(err); // err is of Errors.SecureRecord.InvalidIdStr
    }

    db.findOne({ idStr: idStr }, (err, doc) => {
        if (err) {
            return callback(new Errors.DbError(err));
        }
        if (!doc) {
            return callback(new Errors.RecordNotFound());
        }

        self._keyHash = doc.keyHash;
        self._data = doc.data;

        return callback();
    });
}
/**
 * @callback SecureRecord.fetchByIdStrCb
 * @param {Errors.SecureRecord.InvalidIdStr|Errors.SecureRecord.DbError|Errors.SecureRecord.RecordNotFound} [err]
 */




SecureRecord._isKeyValid = function (key)
{
    if (typeof key !== typeof '' || key.length < 8) {
        return false;
    }
    return true;
}


SecureRecord._createHash = function (str, callback)
{
    async.waterfall([
        bcrypt.genSalt.bind(null, 10),
        (salt, cb) => bcrypt.hash(str, salt, null, cb)
    ], callback);
}


SecureRecord._compareHashes = function (str, hash, callback)
{
    bcrypt.compare(str, hash, callback);
}


SecureRecord._encrypt = function (data, key)
{
    var cipher = crypto.createCipher(ALGORITHM, key);
    return cipher.update(data, 'utf8', 'hex') + cipher.final('hex');
}


SecureRecord._dencrypt = function (data, key)
{
    var decipher = crypto.createDecipher(ALGORITHM, key);
    return decipher.update(data, 'hex', 'utf8') + decipher.final('utf8');
}



module.exports = SecureRecord;
