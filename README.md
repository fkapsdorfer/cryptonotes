
CryptoNotes: Secure & Accessible Notes
======================================

This is a Node.js application for storing and retrieving encrypted notes.
The notes are encrypted using custom encryption key that is not stored on the server.
There is no session management.
The authorization is enforced using global access key which you have to provide in order to upload or decrypt notes.
However, the global key has nothing to do with actual encryption.
The HTTPS is used to prevent any leakage during the network communication.

Certificate installation
------------------------
```sh
openssl req -x509 -newkey rsa:2048 -nodes -keyout server.key -out server.cert
```

Running
-------
```sh
NODE_ENV=[development/production] npm start
```

Todo
----
- idStr could be hashed
