/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file This file defines and exports router for the /crypto path.
 */

'use strict'

var express         = require('express');
var router          = express.Router();
var async           = require('async');
var SecureRecord    = require('../models/SecureRecord');
var Errors          = require('../libs/Errors');


// Authorization middleware for all /crypto/* POSTs.
// GETs don't need to be authorized - those provides forms.
router.use(function (req, res, next) {
    if (req.method === 'POST') {
        var sr = new SecureRecord();
        async.waterfall([
            sr.fetchByIdStr.bind(sr, 'AuthKey'),
            sr.isKeyMatching.bind(sr, req.body.authKey)
        ], (err, authorized) => {
            if (err && !err instanceof Errors.SecureRecord.RecordNotFound) {
                return err.handle(res);
            }
            if (!authorized && !err) {
                return res.status(401).render('error.html', { msg: 'Unauthorized' });
            }

            return next();
        })
    } else {
        return next();
    }
});


router.get('/', (req, res) => {
    return res.render('base.html');
});

router.route('/get')
    .get((req, res) => {
        return res.render('crypto/getForm.html');
    })
    .post((req, res) => {
        var sr = new SecureRecord();
        async.waterfall([
            sr.fetchByIdStr.bind(sr, req.body.idStr),
            sr.getData.bind(sr, req.body.key),
        ], (err, data) => {
            if (err) {
                return err.handle(res);
            }
            return res.render('crypto/getResult.html', { data: data } );
        });
    });

router.route('/set')
    .get((req, res) => {
        return res.render('crypto/setForm.html');
    })
    .post((req, res) => {
        var sr = new SecureRecord(req.body.idStr, req.body.key, req.body.data, (err) => {
            if (err) {
                return err.handle(res);
            }
            sr.persist((err) => {
                if (err) {
                    return err.handle(res);
                }
                return res.render('crypto/setResult.html', { msg: 'Successfully set!' });
            });
        });
    });


module.exports = router;
