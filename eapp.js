/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file This file defines and exports the Express application.
 */

'use strict'

var CONFIG      = require('./libs/config');
var fs          = require('fs');
var express     = require('express');
var bodyParser  = require('body-parser');
var cryptoRoute = require('./routes/crypto');
var nunjucks    = require('nunjucks');
var morgan      = require('morgan');
var eapp        = express();


nunjucks.configure('views', {
    autoescape: true,
    express   : eapp,
    //watch   : true
    noCache   : process.env.NODE_ENV == 'development' ? true : false
});

eapp.use(morgan('dev'));
eapp.use(bodyParser.json());
eapp.use(bodyParser.urlencoded({ extended: false }));

eapp.use('/static', express.static(__dirname + '/static'));
eapp.use('/crypto', cryptoRoute);
eapp.use('/robots.txt', (req, res) => {
    res.render('robots.txt');
});



module.exports = eapp;
