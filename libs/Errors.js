/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file This file defines and exports objects used to identify and handle errors. All errors extends ErrorBase.
 */

'use strict'

var util = require('util');

/**
 * @extends ErrorBase
 */
var Errors = {};


function ErrorBase () {
    // Do not use constructor - so it don't have to be called in the subclass

    // If should be logged
    // this.log = false;

    // Error from lower layer
    // this.err = undefined

    // This will be showed to the user
    // this.msg = 'An error happened.'

    // The http status to send
    // this.status = 500
}

/**
 * Sends the HTTP response and logs error.
 * @param {Response} res
 */
ErrorBase.prototype.handle = function(res) {
    if (this.log) {
        console.error(this);
        if (this.err) {
            console.trace(this.err);
        }
    }
    return res
        .status(this.status ? this.status : 500)
        .render('error.html', { msg: this.msg ? this.msg : 'An error happened.' });
}


Errors.SecureRecord = {
    CannotCreateHash: function(err) {
        this.msg    = 'Cannot create a hash.';
        this.err    = err;
        this.log    = true;
    },
    CannotCompareHashes: function(err) {
        this.msg    = 'Cannot compare a hashes.';
        this.err    = err;
        this.log    = true;
    },
    InvalidKey: function() {
        this.msg    = 'Invalid key: it has to be at least 8 characters long.';
        this.status = 400;
    },
    KeysNotMatching: function(err) {
        this.msg    = 'Bad key.';
        this.status = 400;
    },
    InvalidIdStr: function(err) {
        this.msg    = 'Invalid ID String: it cannot be empty.';
        this.status = 400;
    },
    RecordNotFound: function() {
        this.msg    = 'No such record.';
        this.status = 400;
    },
    DbError: function(err) {
        this.msg    = 'A database error happened.';
        this.err    = err;
        this.log    = true;
    }
}

// Extend all error classes by BaseError
for (var cls in Errors.SecureRecord) {
    if (Errors.SecureRecord.hasOwnProperty(cls) && Errors.SecureRecord[cls] instanceof Function) {
        util.inherits(Errors.SecureRecord[cls], ErrorBase);
    }
}


module.exports = Errors;
