/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file This file loads and exports the configuration.
 */

'use strict'

var fs = require('fs');


var configStr   = fs.readFileSync('config.json', { flag: 'r' });
var CONFIG      = JSON.parse(configStr.toString());

CONFIG.STDOUT   = process.argv[2];


module.exports = CONFIG;
